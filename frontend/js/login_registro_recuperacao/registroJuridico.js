function formatar(mascara, documento) {
    let i = documento.value.length;
    let saida = '#';
    let texto = mascara.substring(i);
    while (texto.substring(0, 1) != saida && texto.length ) {
        documento.value += texto.substring(0, 1);
        i++;
        texto = mascara.substring(i);
    }
    }

$(function () {
    $(document).on("click", "#botaoFormJuridico", function () {
        var vetor_dados = $("#formJuridico").serializeArray();
        var chave_valor = {};
        var emailNaoExiste = false;
        var cnpjValido = false;
        var cnpjNaoExiste = false;
        
        for (var i = 0; i < vetor_dados.length; i++) {
            chave_valor[vetor_dados[i]['name']] = vetor_dados[i]['value'];
        }

        //conversão do cnpj para ASCII, pois o cnpj usa um caractere reservado pela url
        cnpj_codificado = ""
        for (var i =0; i < chave_valor['cnpj'].length; i++) {
            cnpj_codificado += chave_valor['cnpj'].charCodeAt(i)

            // adição de um espaço entre os codigos para o backend diferenciar cada codigo ASCII
            cnpj_codificado += ' '
        }
        
        // verificando se existe o email tentado
        var verificacaoEmail = $.ajax({
            url: `http://localhost:5000/checagem/email/${chave_valor['email']}`,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: dados_json,
            async: false
        });

        verificacaoEmail.done(function (retorno) {
            try {
                if (retorno.existe == "nao") {
                    emailNaoExiste = true;
                } else {
                    alert('email já existe!');
                }
            } catch (error) {
                alert("Erro ao tentar fazer o ajax: " + error +
                    "\nResposta da ação: " + retorno);
            }
        });
        
        // verificacao se o cpnj é valido e não está registrado
        var verificacaoValidadeCnpj = $.ajax({
            url: `http://localhost:5000/checagemMascara/cnpj/${cnpj_codificado}`,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: dados_json,
            async: false
        });

        verificacaoValidadeCnpj.done(function (retorno) {
            try {
                if (retorno.valido == "sim") {
                    cnpjValido = true

                    // se o cnpj for válido, verifica se já foi cadastrado
                    var verificacaoCnpjCadastrado = $.ajax({
                        url: `http://localhost:5000/checagem/cnpj/${cnpj_codificado}`,
                        method: 'POST',
                        dataType: 'json',
                        contentType: 'application/json',
                        data: dados_json,
                        async: false
                    });
        
                    verificacaoCnpjCadastrado.done(function (retorno) {
                        try {
                            if (retorno.existe == "nao") {
                                cnpjNaoExiste = true;
                            } else {
                                alert('cnpj já foi cadastrado!');
                            }
                        } catch (error) {
                            alert("Erro ao tentar fazer o ajax: " + error +
                                "\nResposta da ação: " + retorno);
                        }
                    });
                } else {
                    alert('cnpj inválido!');
                }
            } catch (error) {
                alert("Erro ao tentar fazer o ajax: " + error +
                    "\nResposta da ação: " + retorno);
            }
        });

        // caso todos os requisitos sejam validos realiza o cadastro
        if (emailNaoExiste && cnpjValido && cnpjNaoExiste) {
            var dados_json = JSON.stringify(chave_valor);
            var cadastro = $.ajax({
                url: `http://localhost:5000/registro/Empresa`,
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: dados_json
            });

            cadastro.done(function (retorno) {
                try {
                    if (retorno.resultado == "ok") {
                        alert('registro concluido')
                        window.location.href = "/frontend/html/registro_login/index.html"
                    } else {
                        alert("Erro: " + retorno.detalhes);
                    }
                } catch (error) {
                    alert("Erro ao tentar fazer o ajax: " + error +
                        "\nResposta da ação: " + retorno);
                }
            });
        }
    });
});