function formatar(mascara, documento) {
    let i = documento.value.length;
    let saida = '#';
    let texto = mascara.substring(i);
    while (texto.substring(0, 1) != saida && texto.length ) {
        documento.value += texto.substring(0, 1);
        i++;
        texto = mascara.substring(i);
    }
    }


$(function () {
    $(document).on("click", "#botaoFormFisica", function () {
        var vetor_dados = $("#formJuridico").serializeArray();
        var chave_valor = {};
        var emailNaoExiste = false;
        var cpfValido = false;
        var cpfNaoExiste = false;
        var cpfNaoExiste = false;
        
        for (var i = 0; i < vetor_dados.length; i++) {
            chave_valor[vetor_dados[i]['name']] = vetor_dados[i]['value'];
        }
        var dados_json = JSON.stringify(chave_valor);
        
        // verificando se existe o email tentado
        var verificacaoEmail = $.ajax({
            url: `http://localhost:5000/checagem/email/${chave_valor['email']}`,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: dados_json,
            async: false
        });

        verificacaoEmail.done(function (retorno) {
            try {
                if (retorno.existe == "nao") {
                    emailNaoExiste = true;
                } else {
                    alert('email já existe!');
                }
            } catch (error) {
                alert("Erro ao tentar fazer o ajax: " + error +
                    "\nResposta da ação: " + retorno);
            }
        });
        
        // verificacao se o cpf é valido e não está registrado
        var verificacaoValidadeCpf = $.ajax({
            url: `http://localhost:5000/checagemMascara/cpf/${chave_valor['cpf']}`,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: dados_json,
            async: false
        });

        verificacaoValidadeCpf.done(function (retorno) {
            try {
                if (retorno.valido == "sim") {
                    cpfValido = true

                    // se o cpf for válido, verifica se já foi cadastrado
                    var verificacaoCpfCadastrado = $.ajax({
                        url: `http://localhost:5000/checagem/cpf/${chave_valor['cpf']}`,
                        method: 'POST',
                        dataType: 'json',
                        contentType: 'application/json',
                        data: dados_json,
                        async: false
                    });
        
                    verificacaoCpfCadastrado.done(function (retorno) {
                        try {
                            if (retorno.existe == "nao") {
                                cpfNaoExiste = true;
                            } else {
                                alert('cpf já foi cadastrado!');
                            }
                        } catch (error) {
                            alert("Erro ao tentar fazer o ajax: " + error +
                                "\nResposta da ação: " + retorno);
                        }
                    });
                } else {
                    alert('cpf inválido!');
                }
            } catch (error) {
                alert("Erro ao tentar fazer o ajax: " + error +
                    "\nResposta da ação: " + retorno);
            }
        });

        // verificando se o cnpj inserido existe

        //conversão do cnpj para ASCII, pois o cnpj usa um caractere reservado pela url
        cnpj_codificado = ""
        for (var i =0; i < chave_valor['empresa_id'].length; i++) {
            cnpj_codificado += chave_valor['empresa_id'].charCodeAt(i)
        
            // adição de um espaço entre os codigos para o backend diferenciar cada codigo ASCII
            cnpj_codificado += ' '
        }

        var verificacaoExistenciaCnpj = $.ajax({
            url: `http://localhost:5000/checagem/cnpj/${cnpj_codificado}`,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: dados_json,
            async: false
        });

        verificacaoExistenciaCnpj.done(function (retorno) {
            try {
                if (retorno.existe == "sim") {
                    cnpjExiste = true;
                    
                    // pegando o id da empresa para o backend associar o funcionário a ela
                    var consultaId = $.ajax({
                        url: `http://localhost:5000/consulta/cnpj/${cnpj_codificado}`,
                        method: 'POST',
                        dataType: 'json',
                        contentType: 'application/json',
                        data: dados_json,
                        async: false
                    });

                    consultaId.done(function (retorno) {
                        try {
                            chave_valor['empresa_id'] = retorno.id

                        } catch (error) {
                            alert("Erro ao tentar fazer o ajax: " + error +
                                "\nResposta da ação: " + retorno);
                        }
                    });

                } else {
                    alert('Este cnpj não está cadastrado na empresa!');
                }
            } catch (error) {
                alert("Erro ao tentar fazer o ajax: " + error +
                    "\nResposta da ação: " + retorno);
            }
        });


        // caso todos os requisitos sejam validos realiza o cadastro
        if (emailNaoExiste && cpfValido && cpfNaoExiste && cnpjExiste) {

            var dados_json = JSON.stringify(chave_valor);
            var cadastro = $.ajax({
                url: `http://localhost:5000/registro/Funcionario`,
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: dados_json
            });

            cadastro.done(function (retorno) {
                try {
                    if (retorno.resultado == "ok") {
                        alert('registro concluido')
                        window.location.href = "/frontend/html/registro_login/index.html"
                    } else {
                        alert("Erro: " + retorno.detalhes);
                    }
                } catch (error) {
                    alert("Erro ao tentar fazer o ajax: " + error +
                        "\nResposta da ação: " + retorno);
                }
            });
        }
    });
});