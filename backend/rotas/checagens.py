from config import *
from modelo.pessoa import *
from modelo.empresa import *
from modelo.funcionario import *
from utilidades.validar import validarCnpj,validarCpf

@app.route("/checagem/<string:tipo>/<string:dado>", methods=['GET','POST'])
def checagem(tipo, dado):
    try:
        if tipo == 'email':
            encontradoEmPessoa = Pessoa.query.filter_by(email=dado).first()
            encontradoEmEmpresa = Empresa.query.filter_by(email=dado).first()
            encontradoEmFuncionario = Funcionario.query.filter_by(email=dado).first()
            if encontradoEmPessoa or encontradoEmEmpresa or encontradoEmFuncionario:
                resposta = jsonify({"existe":"sim"})
            else:
                resposta = jsonify({"existe": "nao"})

        if tipo == 'cpf':
            encontrado = Funcionario.query.filter_by(cpf=dado).first()
            if encontrado:
                resposta = jsonify({"existe":"sim3"})
            else:
                resposta = jsonify({"existe": "nao"})

        if tipo == 'cnpj':
            # conversão do código ASCII para o cnpj
            dado = dado.split(' ')
            dado = ''.join(chr(int(codigoASCII)) for codigoASCII in dado)
            encontrado = Empresa.query.filter_by(cnpj=dado).first()
            if encontrado:
                resposta = jsonify({"existe":"sim"})
            else:
                resposta = jsonify({"existe": "nao"})

    except Exception as e:
        resposta = jsonify({"resultado":"erro","detalhes":str(e)})
        print("ERRO: "+str(e))

    return resposta

@app.route("/checagemMascara/<string:tipo>/<string:dado>", methods=['GET','POST'])
def checagemMascara(tipo, dado):
    try:
        if tipo == 'cpf':
            if validarCpf(dado):
                resposta = jsonify({"valido": "sim"})
            else:
                resposta = jsonify({"valido": "nao"})
                
        if tipo == 'cnpj':
            # conversão do código ASCII para o cnpj
            dado = dado.split(' ')
            dado = ''.join(chr(int(codigoASCII)) for codigoASCII in dado)

            if validarCnpj(dado):
                resposta = jsonify({"valido": "sim"})
            else:
                resposta = jsonify({"valido": "nao"})

    except Exception as e:
        resposta = jsonify({"resultado":"erro","detalhes":str(e)})
        print("ERRO: "+str(e))

    return resposta
