from modelo.empresa import *
from modelo.estoque import *
from modelo.pessoa import *
from modelo.funcionario import *

@app.route("/consulta/<string:tipo>/<string:dado>", methods=['GET','POST'])
def consulta(tipo, dado):
    try:
        if tipo == 'cnpj':
                #conversão do código ASCII para o cnpj
                dado = dado.split(' ')
                dado = ''.join(chr(int(codigoASCII)) for codigoASCII in dado)
                encontrado = Empresa.query.filter_by(cnpj=dado).first()
                if encontrado:
                    resposta = jsonify({"id": encontrado.id})
                else:
                    resposta = jsonify({"existe": "nao"})
    except Exception as e:
        resposta = jsonify({"resultado":"erro","detalhes":str(e)})
        print("ERRO: "+str(e))

    return resposta

@app.route("/consultaComLogin/<string:tipo>/<string:dado>", methods=['GET','POST'])
@jwt_required()
def consultaComLogin(tipo, dado):
    try:

        if tipo == 'estoque':
            estoque = Estoque.query.filter_by(id=dado).first()
            resposta = jsonify({'estoque': estoque.informacoes_estoque})

        if tipo == 'email':
            usuario = Pessoa.query.filter_by(email=dado).first()
            if usuario:
                resposta = jsonify({'resultado': 'existe'})
            else:
                resposta = jsonify({'resultado': 'não existe'})

        if tipo == 'estoquesComFuncionario':
            dados = request.get_json()
            empresa_id = Empresa.query.filter_by(email=dados['emailEmpresa']).first().id
            funcionariosEmpresa = Funcionario.query.filter_by(empresa_id=empresa_id).all()

            funcionariosDesteEstoque = {'nomeFuncionario': [], 'cpfFuncionario': []}
            for funcionario in funcionariosEmpresa:
                estoquesFuncionario = list(funcionario.permissoesEstoques)
                
                if dados['idEstoque'] in estoquesFuncionario:
                    funcionariosDesteEstoque['nomeFuncionario'].append(funcionario.nome)
                    funcionariosDesteEstoque['cpfFuncionario'].append(funcionario.cpf)


            resposta = jsonify({'resultado': 'sucesso', 'funcionariosDesteEstoque': funcionariosDesteEstoque})

    except Exception as e:
        resposta = jsonify({"resultado":"erro","detalhes":str(e)})
        print("ERRO: "+str(e))

    return resposta