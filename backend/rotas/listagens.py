from modelo.pessoa import *
from modelo.funcionario import *
from modelo.empresa import *
from modelo.relacionamentos import estoque_pessoa, estoque_empresa
from modelo.estoque import *

@app.route("/listarNoHeader/<string:tipo>/<string:emailRecebidoRota>", methods=['GET','POST'])
@jwt_required()
def listarNoHeader(tipo, emailRecebidoRota):
    try:
        dados = None
        dadosUsuario = None
        if tipo == "pessoa":
            dados = db.session.query(Pessoa).filter_by(email=emailRecebidoRota).first()
            dadosUsuario = dados.json()['nome']
        if tipo == "empresa":
            dados = db.session.query(Empresa).filter_by(email=emailRecebidoRota).first()
            dadosUsuario = dados.json()['nome']
        if tipo == "funcionario":   
            dados = db.session.query(Funcionario).filter_by(email=emailRecebidoRota).first()
            nomeEmpresa = db.session.query(Empresa).filter_by(id= dados.json()['empresa']).first().nome
            dadosUsuario = {'nome':dados.json()['nome'], 'empresa': nomeEmpresa}

        resposta = jsonify({"resultado":"ok","dadosDoHeader":dadosUsuario})

    except Exception as e:
        resposta = jsonify({"resultado":"erro","detalhes":str(e)})
        print("ERRO: "+str(e))

    return resposta

@app.route("/listarNomeEIdEstoques/<string:tipo>/<string:emailRecebidoRota>", methods=['GET','POST'])
@jwt_required()
def listarNomeEstoques(tipo,emailRecebidoRota):
    try:
        if tipo == 'pessoa':
            usuario = Pessoa.query.filter_by(email=emailRecebidoRota).first()

            estoques = [estoque.json()   for estoque in usuario.estoques_pessoa]
            idsLista = [estoque.id for estoque in usuario.estoques_pessoa]

            nomesEstoque = []
            for dicionario in estoques:
                for chave in dicionario:
                    nomesEstoque.append(chave)

        if tipo == 'empresa':
            usuario = Empresa.query.filter_by(email=emailRecebidoRota).first()

            estoques = [estoque.json() for estoque in usuario.estoques_empresa]
            idsLista = [estoque.id for estoque in usuario.estoques_empresa]

            nomesEstoque = []
            for dicionario in estoques:
                for chave in dicionario:
                    nomesEstoque.append(chave)

        resposta = jsonify({'retorno':'sucesso', 'estoques': nomesEstoque, 'ids': idsLista})

    except Exception as e:
        resposta = jsonify({"resultado":"erro","detalhes":str(e)})
        print("ERRO: "+str(e))

    return resposta

@app.route("/listarFuncionarios/<string:emailRecebidoRota>", methods=['GET'])
@jwt_required()
def listarFuncionarios(emailRecebidoRota):
    try:
        empresa = Empresa.query.filter_by(email=emailRecebidoRota).first()
        funcionarios = [funcionario.json() for funcionario in empresa.empregados]
        resposta = jsonify({'resultado':'sucesso', 'funcionarios': funcionarios})

    except Exception as e:
        resposta = jsonify({"resultado":"erro","detalhes":str(e)})
        print("ERRO: "+str(e))

    return resposta