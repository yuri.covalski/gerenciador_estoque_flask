from config import *

class Funcionario(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.Text)
    email = db.Column(db.Text)
    cpf = db.Column(db.String(14))
    senha = db.Column(db.Text)
    empresa_id = db.Column(db.Integer, db.ForeignKey('empresa.id'))
    permissoesEstoques = db.Column(db.Text, default='')
    
    def __str__(self):
        return f'{self.nome} [id={str(self.id)}], ' +\
               f'{self.cpf}'

    def json(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "email": self.email,
            "cpf": self.cpf,
            "senha": self.senha,
            "empresa": self.empresa_id
        }
    
