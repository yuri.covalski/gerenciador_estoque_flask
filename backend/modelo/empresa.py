from config import *
from modelo.relacionamentos import estoque_empresa

class Empresa(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.Text)
    email = db.Column(db.Text)
    cnpj = db.Column(db.String(18))
    senha = db.Column(db.Text)
    empregados = db.relationship('Funcionario', backref='funcionarios')
    estoques_empresa = db.relationship('Estoque', secondary=estoque_empresa, backref='dono')

    def __str__(self):
        return f'{self.nome} [id={str(self.id)}], ' +\
               f'{self.cnpj}'

    def json(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "email": self.email,
            "senha": self.senha,
            "cnpj": self.cnpj,
            "estoques_empresa": self.estoques_empresa
        }