from utilidades.envio_email_com_senha import enviarSenha
from config import *
from modelo.estoque import *
from modelo.empresa import *
from modelo.funcionario import *
from modelo.pessoa import *
from utilidades.criptografar import criptografar

def criar_tabelas():
    with app.app_context():

        if os.path.exists(arquivobd):
            os.remove(arquivobd)

        # criar tabelas
        db.create_all()

def enviar_senha():
    print(enviarSenha('yuregabrielcandido1@gmail.com'))

def buscar():
    with app.app_context():
        funcionario = Empresa.query.filter_by(email='yuregabrielcandido1@gmail.com', senha= criptografar('123')).first()
        if funcionario:
            print(funcionario)

def criar_estoque_json():
    with app.app_context():
        dados = {"chave1": "valor1", "chave2": "valor2"}
        objeto_tabela = Estoque(informacoes_estoque=json.dumps(dados))
        db.session.add(objeto_tabela)
        db.session.commit()

def criar_relacionamento():
    with app.app_context():
        nova_pessoa = Pessoa(nome='João', email='joao@gmail.com', senha='senha123')
        #nova_pessoa2 = Pessoa(nome='João2', email='joao2@gmail.com', senha='senha123')
        #novo_estoque = Estoque(informacoes_estoque=json.dumps('{"estoque": "Informações do Estoque 2"}'))
        #novo_estoque2 = Estoque(informacoes_estoque=json.dumps('{"estoque": "Informações do Estoque 2"}'))
        #novo_estoque3 = Estoque(informacoes_estoque=json.dumps('{"estoque": "Informações do Estoque 2"}'))
        #novo_estoque4 = Estoque(informacoes_estoque=json.dumps('{"estoque": "Informações do Estoque 2"}'))
        #nova_empresa = Empresa(nome='empresa',email='empresa@gmail.com',senha='123',cnpj='123')
        #nova_pessoa.estoques_pessoa.append(novo_estoque)
        #nova_pessoa.estoques_pessoa.append(novo_estoque2)
        #nova_pessoa.estoques_pessoa.append(novo_estoque3)
        #nova_pessoa.estoques_pessoa.append(novo_estoque4)
        #nova_pessoa2.estoques_pessoa.append(novo_estoque)
        #nova_empresa.estoques_empresa.append(novo_estoque)
        db.session.add(nova_pessoa)
        #db.session.add(nova_pessoa2)
        #db.session.add(novo_estoque)
        #db.session.add(nova_empresa)    

        db.session.commit()
        usuario = Pessoa.query.filter_by(id=1).first()
        #print(usuario.estoques_pessoa[0])


criar_tabelas()
with app.app_context():
    empresa = Empresa(nome='gaspa', email='gaspa@gmail.com', cnpj='11.299.826/0001-10', senha='senha')
    funcionario1 = Funcionario(nome='sim', email='joao@gmail.com',cpf='679.928.160-99', senha='123', empresa_id=1)
    funcionario2 = Funcionario(nome='sim2', email='joao2@gmail.com',cpf='104.830.279-28', senha='123', empresa_id=1)
    db.session.add_all([funcionario1, funcionario1, funcionario2, empresa])
    db.session.commit()