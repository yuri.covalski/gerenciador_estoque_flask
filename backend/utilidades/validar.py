import re
from itertools import cycle

def validarCpf(numeroCpf: str) -> bool:
    formato_cpf = re.compile("[0-9][0-9][0-9][.][0-9][0-9][0-9][.][0-9][0-9][0-9][-][0-9][0-9]")
    verificando_padrao = formato_cpf.search(numeroCpf)
    
    if verificando_padrao and len(numeroCpf) == 14:
        cpf = [int(char) for char in numeroCpf if char.isdigit()]
        validade = True

        if cpf == cpf[::-1]:
            validade = False

        for i in range(9, 11):
            value = sum((cpf[num] * ((i+1) - num) for num in range(0, i)))
            digit = ((value * 10) % 11) % 10
            if digit != cpf[i]:
                validade = False
        return validade
    else:
        raise ValueError('Cpf fora do formato desejado, XXX.XXX.XXX-XX')
    
def validarCnpj(cnpj: str) -> bool:
    formato_cnpj = re.compile("[0-9][0-9][.][0-9][0-9][0-9][.][0-9][0-9][0-9][/][0-9][0-9][0-9][1][-][0-9][0-9]")
    verificando_padrao = formato_cnpj.search(cnpj)
    if verificando_padrao and len(cnpj) == 18:

        if cnpj in (c * 18 for c in "1234567890" if c.isdigit()):
            return False
    
        cnpjLimpo = [caractere for caractere in cnpj if caractere.isdigit()]
        cnpj = "".join(cnpjLimpo)
        cnpj_r = cnpj[::-1]
        for i in range(2, 0, -1):
            cnpj_enum = zip(cycle(range(2, 10)), cnpj_r[i:])
            dv = sum(map(lambda x: int(x[1]) * x[0], cnpj_enum)) * 10 % 11
            if cnpj_r[i - 1:i] != str(dv % 10):
                return False

        return True
    else:
        raise ValueError('Cnpj fora do formato desejado, XX.XXX.XXX/0001-XX')